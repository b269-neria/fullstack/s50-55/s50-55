// import Button from 'react-bootstrap/Button';
// import Card from 'react-bootstrap/Card';

// export default function CourseCard() {
//   return (
//     <Card>
//       <Card.Body>
//         <Card.Title>Sample Course</Card.Title>
//         <Card.Text>
//           Description: 
//         </Card.Text>
//         <Card.Text>
//           This is a sample course offering.
//         </Card.Text>
//         <Card.Text>
//           Price:
//         </Card.Text>
//         <Card.Text>
//           PHP 40,000
//         </Card.Text>
        
//         <Button variant="primary">Enroll</Button>
//       </Card.Body>
//     </Card>
//   );
// }

// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';
// import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
// [S50 ACTIVITY END]
    
    // Deconstruct the course properties into their own variables


// [S50 ACTIVITY]
export default function CourseCard({course}) {
        const { name, description, price, _id } = course;

        /*
        Syntax:
            const [getter, setter] = useState(initialGetterValue);
        */
    //     const [count, setCount] = useState(0);

    //     // [S51 Activity]
    //     const [slot, setSlot] = useState(5);

    //     function enroll () {
    //         // if(slot > 0){
    //         // setCount(count + 1)
    //         // setSlot(slot - 1)
    //         //  }else {
    //         //     alert("No more seats available");
    //         //  }
    //         setCount(count + 1)
    //         setSlot(slot - 1)
    //     };
    //     // [S51 Activity end]

    // useEffect(() => {
    //     if(slot <= 0) {
    //         alert("No more seats available!");
    //     }
    // }, [slot]);



return (
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                       {/* <Card.Subtitle>Count: {count}</Card.Subtitle>
                        <Card.Subtitle>Slots: {slot}</Card.Subtitle>*/}
                        {/*<Button variant="primary" onClick={enroll} disabled={slot<=0}>Enroll</Button>*/}
                        <Button className="bg-primary" as= {Link} to={`/courses/${_id}`}>Details</Button>
                    </Card.Body>
                </Card>
            </Col>
    </Row>        
    )
};
// [S50 ACTIVITY END]
