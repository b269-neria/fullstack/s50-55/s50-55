import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register() {
	   const {user} = useContext(UserContext);

	// to store and manage value of the input fields
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [password2, setPassword2] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	// to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);
	const navigate = useNavigate();


	// function to simulate user registration
	function registerUser(e) {
		e.preventDefault();

		// Check if Email exists

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
            })
        })
        .then(res => res.json())
        .then(data => {
            // We will receive either a token or an error response.
            console.log(data);
            if (data === false){
            fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
            	firstName: firstName,
            	lastName: lastName,
                email: email,
                mobileNo: mobileNo,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
        	console.log (data)

        	 Swal.fire({
                    title: "Registration Successful!",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
        	 navigate("/login")

        })

            } else {
                Swal.fire({
                    title: "Duplicate Email Found",
                    icon: "error",
                    text: "Please provide a different email"
                })
            }
        
		})

	};

	useEffect(() => {
		if((email !== "" && password !== "" && password2 !== "" && firstName !== "" && lastName !== "" && mobileNo !== "")&&(password === password2)&&(mobileNo.length >= 11)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	},[email,password,password2, firstName, lastName, mobileNo]);



    return (
    	(user.id !== null) ?
        <Navigate to="/courses"/>
        :
        <Form onSubmit={(e) => registerUser(e)}>
        	<Form.Group controlId="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder="Enter First Name" 
	                value={firstName}
	                onChange= {e => setFirstName(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="userLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder="Enter Last Name" 
	                value={lastName}
	                onChange= {e => setLastName(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                value={email}
	                onChange= {e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="userMobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
	                type="number" 
	                placeholder="Enter Mobile Number" 
	               	value={mobileNo}
	                onChange= {e => setMobileNo(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	               	value={password}
	                onChange= {e => setPassword(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password"
	                value={password2}
	                onChange= {e => setPassword2(e.target.value)} 
	                required
                />
            </Form.Group>

            {isActive ? 
            <Button variant="primary" type="submit" id="submitBtn">
            	Submit
            </Button>
            :
            <Button variant="danger" type="submit" id="submitBtn" disabled>
            	Submit
            </Button>
        	}

        </Form>
    )

}

