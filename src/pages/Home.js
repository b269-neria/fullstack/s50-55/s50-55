// import Banner from '../components/Banner';
// import Highlights from '../components/Highlight';

// export default function Home() {
// 	return (
// 		<>
// 	< Banner
// 	isError= {false}/>
//     < Highlights/>
// 		</>
// 		)
// }

import Banner from '../components/Banner';
import Highlights from '../components/Highlight';


export default function Home() {

	const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere.",
		destination: "/courses",
		label: "Enroll now!"
	}


	return (
		<>
		<Banner data={data} />
    	<Highlights />
    	
		</>
	)
}


